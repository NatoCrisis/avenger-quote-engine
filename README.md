# avenger-quote-engine

AWS Lambda built with the SAM CLI

To build the project locally
- sam build

To run the project locally
- sam local start-api

Sample POST request

```
{
    "character": "ironman",
    "min_characters": 100,
    "max_characters": 115,
    "starting_word": "I" 
}
```

The `starting_word` parameter is optional, however if the Markov engine cannot build a string from that starting word, nothing will be returned.
Good starting words are *I*, *You*, *Yes*, *No* 


