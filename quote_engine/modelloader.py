import json


def load_characters():
    f = open('/opt/python/metadata')
    character_file = f.read()
    f.close()
    raw = json.loads(character_file)
    # For simplicity, I want the keys to be lowercase
    return dict((key.lower(), val) for key, val in raw.items())

def load_model(file_name):
    f = open('/opt/python/' + file_name, 'r')
    model = f.read()
    f.close()
    return model