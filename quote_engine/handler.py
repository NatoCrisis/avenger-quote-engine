import json

from quotegen import QuoteGenerator
from validator import validate
from modelloader import load_characters


def process_post(event):
    characters = load_characters()

    body = json.loads(event['body'])

    msg_params, validation_resp = validate(body, characters)

    is_valid, error_resp = validation_resp
    if is_valid is False:
        return error_resp

    character, min_chars, max_chars = msg_params

    quote = QuoteGenerator(characters).generate_quote(character, body.get('starting_word'), min_chars, max_chars)

    # I prefer to send back a 204 if the quote is empty, otherwise the response has 'quote': null in it
    status_code = 200 if quote is not None else 204
    return {
        "statusCode": status_code,
        "body": json.dumps({
            "quote": quote,
        }),
    }