import markovify
import traceback

from modelloader import load_model

class QuoteGenerator:

    def __init__(self, character_map):
        self.character_map = character_map

    def generate_quote(self, character, starting_word, min_chars, max_chars):

        model_file = self.character_map[character]
        model = load_model(model_file)

        generator = markovify.Text.from_json(model)

        quote = None
        if starting_word is not None:
            try:
                while quote is None:
                    quote = generator.make_sentence_with_start(starting_word, strict=False, min_chars=min_chars,
                                                               max_chars=max_chars)

            except Exception as ex:
                print(f"Could not markovify with '{starting_word}'")
                traceback.print_stack()
        else:
            while quote is None:
                quote = generator.make_short_sentence(min_chars=min_chars, max_chars=max_chars)

        return quote