from handler import process_post


def lambda_handler(event, context):
    # Not really a POST request unless it comes from the API Gateway
    return process_post(event)