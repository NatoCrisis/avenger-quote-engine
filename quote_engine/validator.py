import json

def validate(msg_body, character_map):
    error_list = []
    is_valid = True

    character, character_err = validate_character(msg_body.get('character'), character_map)
    if character_err is not None:
        error_list.append(character_err)
        is_valid = False

    min_chars, max_chars, err_msgs = validate_character_lengths(msg_body)
    if err_msgs:
        error_list.extend(err_msgs)
        is_valid = False

    # TODO:: refactor this nonsense to just return the body portion
    response = {"statusCode": 400,
                "body": json.dumps(
                    {'message': 'The following errors were found', 'errorList': error_list}
                )}
    return (character, min_chars, max_chars), (is_valid, response)


def validate_character(character, character_map):
    err_msg = None
    if character is None:
        err_msg = "Missing parameter: 'character'"
    elif character.lower() not in character_map:
        err_msg = f"Character '{character}' is not currently available"

    return character, err_msg


def validate_character_lengths(msg_body):
    err_msgs = []

    val = None
    min_chars = None
    max_chars = None
    try:
        val = msg_body.get('min_characters')
        min_chars = int(val)
    except TypeError:
        err_msgs.append("Missing parameter: 'min_characters'")
    except ValueError:
        err_msgs.append(f"Invalid min_characters '{val}'")

    try:
        val = msg_body.get('max_characters')
        max_chars = int(val)
    # Passing None to int() throws a TypeError
    except TypeError:
        err_msgs.append("Missing parameter: 'max_characters'")
    except ValueError:
        err_msgs.append(f"Invalid max_characters '{val}'")

    return min_chars, max_chars, err_msgs
